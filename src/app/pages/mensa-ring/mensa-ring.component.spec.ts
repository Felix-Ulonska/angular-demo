import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MensaRingComponent } from './mensa-ring.component';

describe('MensaRingComponent', () => {
  let component: MensaRingComponent;
  let fixture: ComponentFixture<MensaRingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MensaRingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MensaRingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
