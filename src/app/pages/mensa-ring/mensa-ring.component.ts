import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-mensa-ring",
  templateUrl: "./mensa-ring.component.html",
  styleUrls: ["./mensa-ring.component.scss"],
})
export class MensaRingComponent implements OnInit {
  public formGroup = new FormGroup({
    forename: new FormControl<string>("", [
      Validators.min(5),
      Validators.required,
    ]),
    surename: new FormControl<string>("", [
      (control) => {
        const value = control.value;
        if (value.length > 5) {
          return { too_long: true };
        }
        return null;
      },
    ]),
    email: new FormControl<string>("", [Validators.email, Validators.required]),
  });

  constructor() {}

  ngOnInit(): void {
    this.formGroup.valueChanges.subscribe((newVals) => {
      console.log("newVals", newVals);
    });
  }

  submit() {
    console.log("My output", this.formGroup.value);
  }
}
