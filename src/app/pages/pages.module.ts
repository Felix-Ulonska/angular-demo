import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MensaRingComponent } from "./mensa-ring/mensa-ring.component";
import { SharedModule } from "../shared/shared.module";
import { MatCardModule } from "@angular/material/card";
import { ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";

@NgModule({
  declarations: [MensaRingComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatCardModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
  ],
  exports: [MensaRingComponent],
})
export class PagesModule {}
