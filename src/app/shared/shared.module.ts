import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ContentBodyComponent } from "./content-body/content-body.component";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";

const decAndExp = [ContentBodyComponent];

@NgModule({
  declarations: [...decAndExp],
  imports: [CommonModule, MatToolbarModule, MatButtonModule, MatIconModule],
  exports: [...decAndExp],
})
export class SharedModule {}
