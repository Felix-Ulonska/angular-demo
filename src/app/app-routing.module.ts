import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MensaRingComponent } from "./pages/mensa-ring/mensa-ring.component";

const routes: Routes = [
  {
    path: "ring",
    component: MensaRingComponent,
  },
  {
    path: "",
    component: MensaRingComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
