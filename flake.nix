{
  description = "ITPMS-Webiste";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:nixos/nixpkgs";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let pkgs = nixpkgs.legacyPackages.${system}; in
        {
          defaultPackage =
            with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation {
        name = "itpms-site";
        src = ./.;
        buildInputs = [
            nodejs
            node2nix
            #(callPackage ./default.nix {}).shell.nodeDependencies
            #./.
        ];
      };
    });
}
